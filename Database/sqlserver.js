const Connection = require("tedious").Connection;
const Request = require("tedious").Request;
const Util = require("util");

const connectionSQLS = new Connection({
  userName: "",
  password: "",
  server: "",
  options: {
    database: "",
  },
});

connectionSQLS.on("connect", (err) => {
  if (err) throw err;
});

//For Select ALL and Insert Only
export const querySAI = (query) => {
  //e.g "SELECT * FROM (table name)"
  //e.g "INSERT INTO (table name) ((columns)) OUTPUT INSERTED.(column) VALUES ((values))"
  let request = new Request(query, (err, rowCount, rows) => {
    if (err) throw err;
  });

  let results = [];
  request.on("row", (column) => {
    results.push(column);
  });

  request.on("doneProc", () => {
    process.exit();
  });

  return connectionSQLS.execSql(request);
};

//For Select, Update, and Delete
export const querySUD = (query) => {
  //e.g "SELECT * FROM (table name) WHERE (column) = %d", (value)
  //e.g "UPDATE (table name) SET (column selected)=%s WHERE (column condition)=%d", value
  //e.g "DELETE FROM (table name) WHERE (column) = %d", value
  let request = new Request(Util.format(query), (err, rowCount, rows) => {
    if (err) throw err;
  });

  request.on("doneProc", () => {
    process.exit(1);
  });

  return connectionSQLS.execSql(request);
};
