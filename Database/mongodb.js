import * as Mongoose from "mongoose";
import { Schema } from "mongoose";

export const connectMDB = (uri) => {
  if (Mongoose.Connection) {
    return;
  }

  Mongoose.connect(uri, {
    useNewUrlParser: true,
    useFindAndModify: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  });

  let dbConnect = Mongoose.connection;

  dbConnect.once("open", async () => {
    console.log("Connect MongoDB Success!");
  });

  dbConnect.on("error", () => {
    alert("Error Connect MongoDB");
    console.log("Error Connect MongoDB");
  });
};

//See MongoDB or Mongoose documentation
export const userSchema = (data) => {
  return new Schema({ data });
};
