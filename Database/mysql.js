//@ts-nocheck
import * as mysql from "mysql";

const connection = mysql.createConnection({
  host: "",
  user: "",
  password: "",
  database: "",
});
connection.connect((err) => {
  if (err) throw err;
  alert("Connect MySQL Success");
  console.log("Connected");
});

export const queryMySQL = (query) => {
  //e,g 'SELECT * FROM (table name)'
  //e.g 'INSERT INTO (table name) SET ?', value
  //e.g 'UPDATE (table name) SET (column selected) = ? Where (column condition) = ?', [value, value]
  //e.g  'DELETE FROM (table name) WHERE (column) = ?', [value]
  return connection.query(query, (err, rows) => {
    if (err) throw err;
  });
};
