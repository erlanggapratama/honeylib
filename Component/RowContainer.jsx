import React from "react";

export default (props) => {
  return (
    <div
      {...props}
      style={{
        display: "flex",
        flex: 1,
        flexDirection: "row",
        flexWrap: "wrap",
      }}
    ></div>
  );
};
