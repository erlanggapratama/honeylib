import Container from "./Container";
import RowContainer from "./RowContainer";
import EditText from "./EditText";
import Button from "./Button";
import Form from "./Table/Form";
import Modal from "./Modal";
import Text from "./Text";
import Table from "./Table";
import TableColumn from "./Table/TableColumn";
import Field from "./Table/Field";

Container.displayName = "Container";
RowContainer.displayName = "RowContainer";
EditText.displayName = "EditText";
Button.displayName = "Button";
Text.displayName = "Text";
TableColumn.displayName = "TableColumn";
Form.displayName = "Form";
Field.displayName = "Field";

export {
  Container,
  RowContainer,
  EditText,
  Button,
  Form,
  Modal,
  Text,
  Table,
  TableColumn,
  Field,
};
