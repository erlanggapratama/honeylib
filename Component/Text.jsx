import React from "react";
import "./w3.css";

export default (props) => {
  const { size, children } = props;
  let sizeText = "w3-medium";
  if (size) {
    if (size === "medium") {
      sizeText = "w3-medium";
    } else if (size === "large") {
      sizeText = "w3-xlarge";
    } else if (size === "big") {
      sizeText = "w3-xxlarge";
    } else if (size === "jumbo") {
      sizeText = "w3-xxxlarge";
    }
  }

  return (
    <div>
      <span {...props} className={sizeText}>
        {children}
      </span>
    </div>
  );
};
