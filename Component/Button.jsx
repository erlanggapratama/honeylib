import React from "react";
import "./w3.css";

export default (props) => {
  const { color, disabled, children, border, wide } = props;
  let classStyle = "w3-button w3-medium";
  if (color) {
    classStyle += ` w3-${color}`;
  } else {
    classStyle += ` w3-blue`;
  }

  if (border) {
    classStyle = `w3-button w3-medium  w3-white w3-border w3-border-${border} w3-hover-${border}`;
  }

  if (wide) {
    classStyle += ` w3-block`;
  }

  return (
    <div style={{ margin: 10 }}>
      <button {...props} className={classStyle} disabled={disabled}>
        {children}
      </button>
    </div>
  );
};
