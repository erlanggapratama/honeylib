import React from "react";
import Container from "./Container";
import "./w3.css";
import Button from "./Button";

export default (props) => {
  const { id, children, open } = props;
  const [close, setClose] = React.useState({
    display: "none",
  });

  React.useEffect(() => {
    if (open) {
      setClose({ display: "block" });
    } else {
      setClose({ display: "none" });
    }
  }, [open]);

  return (
    <div id={id} className="w3-modal" style={close}>
      <div className="w3-modal-content">
        <div className="w3-container">
          <Container>
            {children}
            <div className="w3-right-align">
              <Button onClick={() => setClose({ display: "none" })} color="red">
                Close
              </Button>
            </div>
          </Container>
        </div>
      </div>
    </div>
  );
};
