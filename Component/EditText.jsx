import React from "react";
import "./w3.css";

export default (props) => {
  const { title, placeholder, prefix, suffix } = props;
  const [value, setValue] = React.useState(null);

  return (
    <div style={{ margin: 10 }}>
      <label style={{ marginBottom: 10 }}>{title}</label>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          width: 310,
        }}
        className="w3-border"
      >
        {prefix && <span style={{ padding: 10 }}>{prefix}</span>}
        <input
          style={{ padding: 10 }}
          {...props}
          className="w3-input w3-border-0"
          placeholder={placeholder}
        ></input>
        {suffix && <span style={{ padding: 10 }}>{suffix}</span>}
      </div>
    </div>
  );
};
