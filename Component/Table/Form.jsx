import React, { useContext } from "react";
import Container from "../Container";
import RowContainer from "../RowContainer";
import Button from "../Button";
import Modal from "../Modal";
import { useParams, navigate } from "@reach/router";
import "../w3.css";

export default (props) => {
  const { formTitle, items, children, api, removeDelete, route } = props;
  const params = useParams();
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    if (open) {
      setOpen(false);
    } else {
      setOpen(true);
    }
  };

  const handleInput = async () => {
    if (!params.id) {
      const result = await input(api);
      if (result) {
        navigate(`/${route}/Table/`);
        window.location.reload();
      }
    }
  };

  //TODO: Handle Delete
  const handleDelete = async () => {};

  const handleCancel = () => {
    navigate(-1);
  };

  return (
    <Container>
      <form onSubmit={(event) => event.preventDefault()}>
        <div
          style={{
            margin: 10,
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <h3>{formTitle}</h3>
          <div className="w3-hide-large w3-hide-medium">
            <Button border="blue" onClick={handleOpen}>
              Options
            </Button>
            <Modal id="option" open={open}>
              <Button
                border="blue"
                wide
                style={{ padding: 10 }}
                onClick={handleInput}
              >
                Submit
              </Button>
              {!removeDelete && (
                <Button
                  border="red"
                  wide
                  style={{ padding: 10 }}
                  onClick={handleDelete}
                >
                  Delete
                </Button>
              )}

              <Button
                border="yellow"
                wide
                style={{ padding: 10 }}
                onClick={handleCancel}
              >
                Cancel
              </Button>
            </Modal>
          </div>
          <div
            className="w3-container w3-hide-small"
            style={{ display: "flex", flexDirection: "row" }}
          >
            <Button border="green" onClick={handleInput}>
              Submit
            </Button>
            {!removeDelete && (
              <Button border="red" onClick={handleDelete}>
                Delete
              </Button>
            )}

            <Button border="yellow" onClick={handleCancel}>
              Cancel
            </Button>
          </div>
        </div>
        <RowContainer>{children}</RowContainer>
      </form>
    </Container>
  );
};

const input = async (api) => {
  return await fetch(api.url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(api.body),
  });
  // .then((respond) => {
  //   console.log(respond.data);
  //   navigate(`${formTitle}/Table/`);
  // })
  // .catch((err) => console.log(err));
};
