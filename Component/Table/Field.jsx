import React from "react";
import "../w3.css";

export default (props) => {
  const {
    title,
    placeholder,
    prefix,
    suffix,
    path,
    selector,
    boolean,
    foreign,
  } = props;
  let option;

  if (boolean) {
    option = boolean.split("/");
  }

  let obj = 1;
  if (foreign) {
    foreign.map((itm) => {
      let i = 0;
      for (let it in itm) {
        let oby = it.split("_");
        if (oby[1] === ("nama" || "name")) {
          obj = i;
          break;
        }
        i++;
      }
    });
  }
  return (
    <div style={{ margin: 10 }}>
      <label style={{ marginBottom: 10 }}>{title}</label>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          width: 310,
        }}
        className="w3-border"
      >
        {selector ? (
          selector === "boolean" && boolean ? (
            <select className="w3-select" {...props}>
              <option value={0}>{option[1]}</option>
              <option value={1}>{option[0]}</option>
            </select>
          ) : (
            selector === "multiple" &&
            foreign && (
              <select className="w3-select" {...props}>
                {foreign.map((values) => (
                  <option value={Object.values(values)[0]}>
                    {Object.values(values)[obj]}
                  </option>
                ))}
              </select>
            )
          )
        ) : (
          <>
            {prefix && <span style={{ padding: 10 }}>{prefix}</span>}
            <input
              style={{ padding: 10 }}
              {...props}
              className="w3-input w3-border-0"
              placeholder={placeholder}
            ></input>
            {suffix && <span style={{ padding: 10 }}>{suffix}</span>}
          </>
        )}
      </div>
    </div>
  );
};
