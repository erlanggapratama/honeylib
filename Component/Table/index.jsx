import React, { useState, useEffect } from "react";
import Container from "../Container";
import RowContainer from "../RowContainer";
import Text from "../Text";
import EditText from "../EditText";
import Button from "../Button";
import Card from "../Card";
import { navigate } from "@reach/router";
import "../w3.css";

export default (props) => {
  const { data, children, title, route } = props;

  const [sort, setSort] = useState({
    arrData: [],
    condition: false,
  });

  const [search, setSearch] = useState("");

  const handleSort = () => {
    if (sort.condition) {
      setSort({ arrData: sort.arrData.sort(sortAsc), condition: false });
      console.log(sort.arrData);
    } else {
      setSort({ arrData: sort.arrData.sort(sortDesc), condition: true });
      console.log(sort.arrData);
    }
  };

  let objIdx;
  data.map((el) => {
    let i = 0;
    for (let it in el) {
      let oby = it.split("_");
      if (oby[1] === ("nama" || "name")) {
        objIdx = i;
        break;
      }
      i++;
    }
  });

  useEffect(() => {
    if (sort.arrData.length === 0) {
      setSort({ arrData: data, condition: false });
    }
  });

  return (
    <Container>
      <div
        style={{
          display: "flex",
          padding: "10px 10px 10px",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Text size="large" style={{ fontWeight: "bold" }}>
          {title}
        </Text>
        <Button onClick={() => navigate(`/${route}/InsertForm/`)} border="blue">
          Add
        </Button>
      </div>
      <Container>
        <RowContainer>
          <EditText
            placeholder="search"
            value={search}
            onChange={(event) => setSearch(event.target.value.toLowerCase())}
          />
          <Button border="blue" onClick={handleSort}>
            {sort.condition ? "Sort Ascending" : "Sort Descending"}
          </Button>
        </RowContainer>
        {/* For Mobile */}
        <div className="w3-hide-large w3-hide-medium">
          {sort.arrData.length > 0 &&
            sort.arrData.map((item) => (
              <Card
                style={{ padding: 10, margin: 10 }}
                onClick={() =>
                  navigate(`/${route}/EditForm/${Object.values(item)[0]}`, {
                    state: { data: item },
                  })
                }
              >
                <Container>
                  <Text style={{ fontWeight: "bold" }}>
                    {Object.values(item)[objIdx]}
                  </Text>
                  {/* <Text>{Object.values(item)[2]}</Text> */}
                </Container>
              </Card>
            ))}
        </div>

        {/* For Web */}
        <table className="w3-table w3-bordered w3-hide-small">
          <thead>
            <tr>
              {children.map((item) => (
                <th>{item.props.title} </th>
              ))}
            </tr>
          </thead>
          <tbody>
            {data.length > 0 &&
              data.map((items) => {
                if (
                  search &&
                  Object.values(items)[objIdx].toLowerCase().includes(search)
                ) {
                  return ContentTable(route, children, items, objIdx);
                } else if (search === "" || !search) {
                  return ContentTable(route, children, items, objIdx);
                }
              })}
          </tbody>
        </table>
      </Container>
    </Container>
  );
};

const ContentTable = (route, children, items) => (
  <tr
    key={Object.values(items)[0]}
    onClick={() =>
      navigate(`/${route}/EditForm/${Object.values(items)[0]}`, {
        state: { data: items },
      })
    }
  >
    {children.map((item) => {
      if (Object.keys(items).includes(item.props.path)) {
        if (item.props.boolean) {
          let option = item.props.boolean.split("/");
          return (
            <td>
              {items[item.props.path] === 0 ? (
                <label>{option[1]}</label>
              ) : (
                <label>{option[0]}</label>
              )}
            </td>
          );
        } else if (item.props.foreign) {
          let value;
          let objIdx;
          item.props.foreign.map((el) => {
            let i = 0;
            for (let it in el) {
              let oby = it.split("_");
              if (oby[1] === ("nama" || "name")) {
                objIdx = i;
                break;
              }
              i++;
            }
            if (Object.values(el)[0] === items[item.props.path]) {
              value = Object.values(el)[objIdx];
            }
          });
          return (
            <td>
              <label>{value}</label>
            </td>
          );
        } else {
          return (
            <td>
              <label>{items[item.props.path]}</label>
            </td>
          );
        }
      } else {
        return <td></td>;
      }
    })}
  </tr>
);

const sortAsc = (a, b) => {
  let objIdx;
  for (let it in a) {
    let oby = it.split("_");
    if (oby[1] === ("nama" || "name")) {
      objIdx = it;
      break;
    }
  }
  console.log(a, b);
  if (a[objIdx] < b[objIdx]) {
    return -1;
  }
  if (a[objIdx] > b[objIdx]) {
    return 1;
  }
  return 0;
};

const sortDesc = (a, b) => {
  let objIdx;
  for (let it in a) {
    let oby = it.split("_");
    if (oby[1] === ("nama" || "name")) {
      objIdx = it;
      break;
    }
  }
  if (a[objIdx] > b[objIdx]) {
    return -1;
  }
  if (a[objIdx] < b[objIdx]) {
    return 1;
  }
  return 0;
};
