import React from "react";
import "./w3.css";

export default (props) => {
  const { children } = props;
  return (
    <div {...props} className="w3-card">
      {children}
    </div>
  );
};
